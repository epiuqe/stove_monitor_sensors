#include <RFM69.h>
#include <RFM69_ATC.h>
#include <SPIFlash.h>
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneButton.h>
#include <max6675.h>

#define NODEID 2
#define NETWORKID 100
#define GATEWAYID 1
#define FREQUENCY RF69_868MHZ
#define ENCRYPTKEY "sampleEncryptKey"
#define IS_RFM69HW_HCW
#define ATC_RSSI -80
#define SERIAL_BAUD 115200
#define thermoSO 7
#define thermoCS 6
#define thermoSCK 4

OneWire oneWire(5);
DallasTemperature sensors(&oneWire);
DeviceAddress boiler = {0x28, 0xBC, 0x17, 0xD6, 0x8, 0x0, 0x0, 0x64};
DeviceAddress stove = {0x28, 0xC3, 0xFE, 0xD6, 0x8, 0x0, 0x0, 0xF1};
DeviceAddress stove_return = {0x28, 0x94, 0xFA, 0xD6, 0x8, 0x0, 0x0, 0x45};
char boiler_str[8];
char stove_str[8];
char stove_return_str[8];
char thermocouple_str[10];
char message_to_sent[40];
unsigned long lastTempRequest = 0;
int delayInMillis = 0;
OneButton button(8, true);
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
char lcd_first_line[9];
char lcd_second_line[9];
// int lcd_refresh_period = 1000;
long last_lcd_period = 0;
byte lcd_change_line;
RFM69_ATC radio;
int TRANSMITPERIOD = 30000;
long lastPeriod = 0;
MAX6675 thermocouple;

void incrementLcdCounter()
{
  lcd_change_line++;
}

void setup()
{
  Serial.begin(SERIAL_BAUD);

  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  radio.setHighPower();
  radio.encrypt(ENCRYPTKEY);
  radio.enableAutoPower(ATC_RSSI);

  sensors.begin();
  sensors.setWaitForConversion(false);
  sensors.requestTemperatures();
  delayInMillis = 750;
  lastTempRequest = millis();
  sensors.setResolution(12);

  thermocouple.begin(thermoSCK, thermoCS, thermoSO);

  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Starting");

  pinMode(8, INPUT_PULLUP);
  button.attachClick(incrementLcdCounter);

  delay(1000);
}

void loop()
{
  button.tick();
  unsigned long currentMillis = millis();
  int currPeriod = currentMillis / TRANSMITPERIOD;

  if (radio.receiveDone())
  {
    Serial.print('[');
    Serial.print(radio.SENDERID, DEC);
    Serial.print("] ");
    for (byte i = 0; i < radio.DATALEN; i++)
      Serial.print((char)radio.DATA[i]);
    Serial.print("   [RX_RSSI:");
    Serial.print(radio.RSSI);
    Serial.print("]");

    if (radio.ACKRequested())
    {
      radio.sendACK();
      Serial.print(" - ACK sent");
    }
    Serial.println();
  }

  if (millis() - lastTempRequest >= delayInMillis)
  {
    dtostrf(sensors.getTempC(boiler), 2, 2, boiler_str);
    dtostrf(sensors.getTempC(stove), 2, 2, stove_str);
    dtostrf(sensors.getTempC(stove_return), 2, 2, stove_return_str);
    dtostrf(thermocouple.readCelsius(), 2, 2, thermocouple_str);

    sensors.requestTemperatures();
    lastTempRequest = millis();
  }

  if (lcd_change_line % 4 == 0)
  {
    sprintf(lcd_first_line, "K%7s", stove_str);
    sprintf(lcd_second_line, "S%7s", thermocouple_str);
  }
  else if (lcd_change_line % 4 == 1)
  {
    sprintf(lcd_first_line, "S%7s", thermocouple_str);
    sprintf(lcd_second_line, "B%7s", boiler_str);
  }
  else if (lcd_change_line % 4 == 2)
  {
    sprintf(lcd_first_line, "B%7s", boiler_str);
    sprintf(lcd_second_line, "P%7s", stove_return_str);
  }
  else
  {
    sprintf(lcd_first_line, "P%7s", stove_return_str);
    sprintf(lcd_second_line, "K%7s", stove_str);
  }
  lcd.setCursor(0, 0);
  lcd.print(lcd_first_line);
  lcd.setCursor(0, 1);
  lcd.print(lcd_second_line);

  if (currPeriod != lastPeriod)
  {
    lastPeriod = currPeriod;

    sprintf(message_to_sent, "%s|%s|%s|%s", boiler_str, stove_str, stove_return_str, thermocouple_str);
    int bf_length = strlen(message_to_sent);
    Serial.print("Sending[");
    Serial.print(bf_length);
    Serial.print("]: ");
    Serial.print(message_to_sent);

    if (radio.sendWithRetry(GATEWAYID, message_to_sent, bf_length))
      Serial.print(" ok!");
    else
      Serial.print(" nothing...");
    Serial.println();
  }
}